//
//  CustomPDFViewController.swift
//  DocumentEditorPoC
//
//  Created by Alin Muntean on 03.04.2024.
//

import PSPDFKitUI

class CustomPDFViewController: PDFViewController, PDFViewControllerDelegate, PDFDocumentEditorDelegate {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.delegate = self
    let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(save))
    var documentEditorRightItems = navigationItem.rightBarButtonItems(for: .documentEditor)
    documentEditorRightItems?.append(saveButton)
    navigationItem.setRightBarButtonItems(documentEditorRightItems, for: .documentEditor, animated: false)
  }
  
  func pdfViewController(_ pdfController: PDFViewController, didChange viewMode: ViewMode) {
    if viewMode == .documentEditor {
      pdfController.documentEditorController.documentEditor?.add(self)
    } else {
      pdfController.documentEditorController.documentEditor?.remove(self)
    }
  }
  
  func documentEditor(_ editor: PDFDocumentEditor, didPerform changes: [PDFEditingChange]) {
    print("Performed changes from \(self)")
  }
  
  func documentEditorRequestsFullReload(_ editor: PDFDocumentEditor) {
    print("Reset from \(self)")
  }
  
  @objc func save() {
    print("Saving document...")
    documentEditorController.documentEditor?.save() { document, error in
      print("Saved document editor: \(String(describing: document)), error: \(String(describing: error))")
      
      document?.save(options: [.applyRedactions]) { result in
        switch result {
        case .success(let annotations): print("Saved document annotations: \(annotations)")
        case .failure(let error): print("Error saving document annotations: \(error)")
        }
      }
    }
  }
}

extension CustomPDFViewController {
  
  class CustomDocumentEditorViewController: PDFDocumentEditorViewController {
    
    override func documentEditor(_ editor: PDFDocumentEditor, didPerform changes: [PDFEditingChange]) {
      super.documentEditor(editor, didPerform: changes)
      print("Performed changes from \(self)")
    }
    
    override func documentEditorRequestsFullReload(_ editor: PDFDocumentEditor) {
      super.documentEditorRequestsFullReload(editor)
      print("Reset from \(self)")
    }
  }
}
