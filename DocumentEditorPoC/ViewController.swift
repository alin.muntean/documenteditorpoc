//
//  ViewController.swift
//  DocumentEditorPoC
//
//  Created by Alin Muntean on 03.04.2024.
//

import UIKit
import PSPDFKitUI

class ViewController: UIViewController {
  
  @IBAction func pushCustomVC() {
    
    let fileUrl: URL! = Bundle.main.url(forResource: "file", withExtension: "pdf")
    let writableUrl = FileHelper.copyFileURLToDocumentDirectory(fileUrl, overwrite: true)
    let document = Document(url: writableUrl)
    let controller = CustomPDFViewController(document: document) {
      $0.overrideClass(PDFDocumentEditorViewController.self,
                       with: CustomPDFViewController.CustomDocumentEditorViewController.self)
    }
    navigationController?.pushViewController(controller, animated: true)
  }
}

class FileHelper: NSObject {
  
  class func copyFileURLToDocumentDirectory(_ documentURL: URL, overwrite: Bool) -> URL {
    // Copy file from original location to the Document directory (a location we can write to).
    let docsFolder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let newURL = docsFolder.appendingPathComponent(documentURL.lastPathComponent, isDirectory: false)
    let exists = FileManager.default.fileExists(atPath: newURL.path)
    if overwrite {
      do {
        try FileManager.default.removeItem(at: newURL)
      } catch CocoaError.fileNoSuchFile, CocoaError.fileReadNoSuchFile {
        // The file not existing doesn’t need reporting as an error since that’s what we want anyway.
      } catch {
        print("Error while removing file at \(newURL.path): \(error.localizedDescription)")
      }
    }
    
    if !exists || overwrite {
      do {
        try FileManager.default.copyItem(at: documentURL, to: newURL)
      } catch {
        print("Error while copying \(documentURL.path): \(error.localizedDescription)")
      }
    }
    
    return newURL
  }
}

